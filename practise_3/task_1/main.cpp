#include <iostream>
#include <string>

using namespace std;


void convertNumber(int x, int y) {
    int a[10], i;

    for(i = 0; x > 0; i++) {
        a[i] = x % y;
        x = x / y;
    }

    for(i = i - 1; i >= 0; i--) {
        cout << a[i];
    } 
}


int main() {

    int x, y;
    cout << "Enter number (DEC):";
    cin >> x;

    cout << "Enter number system (MAX HEX):";
    cin >> y;

    if(y > 16) {
        cout << "Undefined number system";
    } else {
        convertNumber(x, y);
    } 


    return 0;
}