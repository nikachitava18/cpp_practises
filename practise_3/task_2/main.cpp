#include <iostream>
#include <string>

using namespace std;


double taxCalculation(double price, int tax) {
    double payment = price - (price * tax / 100);
    return payment;
}

double taxCalculation(double price) {
    double payment = price - (price * 0.18);
    return payment;
}

int main() {

    cout << "Your payment is: " << taxCalculation(100, 4) << endl;
    cout << "Your payment is: " << taxCalculation(100) << endl;

    return 0;
}