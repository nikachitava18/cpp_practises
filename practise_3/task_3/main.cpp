#include <iostream>
#include <string>
#include <cmath>

using namespace std;

void secondConverter(int seconds) {
    int minutes = seconds / 60;
    int hours = minutes / 60;
    cout << seconds << " seconds is " << hours << " hours and " << int(seconds%60) << " minutes" << endl;
}

int main() {
    
    int x;
    cout << "Enter seconds: ";
    cin >> x;
    secondConverter(x);

    //cout << secondConverter(x) << endl;

    return 0;
}