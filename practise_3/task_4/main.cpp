#include <iostream>
#include <string>
#include <ctime>


using namespace std;

int main() {
    string words[5] = {"lecture", "computer", "software", "hardware", "screen"};
    
    srand(time(NULL));
    int wordNumber = rand() % 5;

    string guessingWord = words[wordNumber];
    int wordLenght = guessingWord.length();
    char hashedWorld[wordLenght];
    for(int i = 0; i < wordLenght; i++) {
        hashedWorld[i] = '*';
    }
    // cout << "The word is: " << guessingWord << endl;

    int life = wordLenght * 2;
    int score_to_win = wordLenght;
    int score;

    cout << "You have: " << life << " life" << endl;
    cout << "Your guessing word is: ";
    for(int i = 0; i < wordLenght; i++) {
        cout << hashedWorld[i];
    }
    cout << endl;

    cout << "To exit game enter: !" << endl;

    char guessletter; 

    for(int i = 0; i < wordLenght * 2; i++) {
        cout << "You have " << life << " try to guess the word. " << endl;
        cout << "Enter letter to guess word: ";
        cin >> guessletter;

        if(guessletter == '!') {
            cout << "You exit from game. You lose" << endl;
            break;
        }

        for(int j = 0; j < wordLenght; j++) {
            if(guessletter == guessingWord.at(j)) {
                hashedWorld[j] = guessletter;
                cout << "You guess the letter." << endl;
                cout << "Now, your word look: ";
                for(int k = 0; k < wordLenght; k++) {
                    cout << hashedWorld[k];
                }
                score ++;
                cout << endl;
            }
        }

        if(score == score_to_win) {
            cout << "Congretulation, you guess the word and won game.";
            break;
        }

        life--;
        if(life == 0) {
            cout << "You can't guess the world. You lose.";
        }
    }


    return 0;
}