#include <iostream>
#include <string>

using namespace std;

/*
    ეკრანზე მომხმარებელს შეუძლია შეიყვანოს სიმბოლოები მანამ სანამ არ აკრეფს სიმბოლო "!". 
    თითოუელ სიმბოლოზე დაბეჭდეთ ის არის ხმოვანი თუ არა.
*/


int main() {

    char a;

    while(a != '!') {
        cout << "Enter a symbol:";
        cin >> a;
    }
    
    return 0;
}