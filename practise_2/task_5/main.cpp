#include <iostream>
#include <string>

using namespace std;

/*
    შემოდის ტექტსი. 
    გაიგეთ პალინდრომი არის თუ არა ის. 
    (პალინდრომია ტექტსი რომელიც უკუღმა წაკითხვითაც იგივე მნიშვნელობას ვიღებთ.
    მაგალითად: abba არის პალინდრომი და abc არა. 
*/

int main() {

    string text, reversed;
    bool isPol = false;

    cout << "Enter text: ";
    cin >> text;

    for(int i = 0; i < text.length(); i++) {
        if(text.at(i) == text.at(text.length()-i-1)) {
            isPol = true;
        } else {
            isPol = false;
        }  
    } 

    if(isPol) {
        cout << "Palindrom";
    } else {
        cout << "No Palindrom";
    }


    return 0;
}