#include <iostream>
#include <string>

using namespace std;

/*
    სტუდენტს შეჰყავს საგნების რაოდენობა და საგნების ქულა. 
    გაიგეთ შეყვანილი ქულებიდან უმაღლესი და ყველაზე დაბალი ქულა. 
    გამოთვალეთ საშუალო და დაბეჭდეთ ეს მნიშვნელობები ეკრანზე.
*/

int main() {

    int a;
    cout << "Enter your subjects quantity: ";
    cin >> a;

    int grades[a];

    for(int i = 0; i < a; i++) {
        cout << "Enter your grade in " << i + 1 << " subject: ";
        cin >> grades[i];
    }

    int maxGrade, minGrade;
    maxGrade = grades[0];
    minGrade = grades[0];

    double avrGrade;
    int totalGrade;

    for(int i = 0; i < a; i++) {
        if(grades[i] > maxGrade) {
            maxGrade = grades[i];
        }
        if(grades[i] < minGrade) {
            minGrade = grades[i];
        }
        totalGrade += grades[i];
    }

    avrGrade = totalGrade / a;

    cout << endl;
    cout << "Average grade: " << avrGrade << endl;
    cout << "Max grade: " << maxGrade << endl;
    cout <<  "Min grade: " << minGrade << endl;
    cout << endl;
    
    return 0;
}