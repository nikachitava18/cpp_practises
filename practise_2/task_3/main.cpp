#include <iostream>
#include <string>

using namespace std;

/*
    მომხმარებელს შეჰყავს ოცი ცალი პროდუქტის ფასი. ამის შემდეგ დაბეჭდეთ ეკრანზე ისეთი პროდუქტის ფასები, 
    რომლებიც ნაკლებია 5-ზე. შემდეგ გამოთვალეთ საშუალო ფასი და დაბეჭდეთ საშუალოზე მაღალი ღირებულების ფასები.
*/

int main() {

    int prodPrice[20];
    int totalPrice;
    double avrgPrice;

    for(int i = 0; i < 20; i++) {
        cout << "Enter product " << i+1 << " price: ";
        cin >> prodPrice[i];
    }

    cout << endl;
    cout << "Less price than 5$:" << endl;
    cout << endl;

    for(int i = 0; i < 20; i++) {
        if(prodPrice[i] < 5) {
            cout << "Prod: " << i + 1 << " Price $: " << prodPrice[i] << endl;
        }
        totalPrice += prodPrice[i];
    }

    avrgPrice = totalPrice / 20;

    cout << endl;
    cout << "Average price $: " << avrgPrice << endl;

    cout << endl;

    cout << "Above than average price:" << endl;
    cout << endl;

    for(int i = 0; i < 20; i++) {
        if(prodPrice[i] > avrgPrice) {
            cout << "Prod: " << i + 1 << " Price $: " << prodPrice[i] << endl;
        }
    }
    
    return 0;
}