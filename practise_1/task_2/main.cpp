/*
Task 2
    სტუდენტი არაა ნებადართული რომ დაესწროს გამოცდას თუ გაცდენილი აქვს 75%-ზე მეტი ლექციების. 
    აღწერეთ ორი ცვლადი. ერთი ასახავს სულ ლექციების რაოდენობას, მეორე რამდენს დაესწრო სტუდენტი. 
    განსაზღვრეთ შეუძლია თუ არა სტუდენტს გამოცდაზე დასწრება ?
*/

#include <iostream>
#include <string>

using namespace std;


int main() {
    
    int lecture; //number of lecture
    int attend;  //number of attended lecture

    cout << "Enter number of lecture: ";
    cin >> lecture;

    cout << "Enter number of attended lecture: ";
    cin >> attend;
    
    double barrier = lecture * 0.75;

    if(barrier <= attend) {
        cout << "You are allowed to pass exam." << endl;
    } else {
        cout << "Your are not allowed to pass an exam. " << endl;
    }

    return 0;
}