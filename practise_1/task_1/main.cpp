/*
Task 1
    აღწერეთ ცლადი რომელი ასახავს სახელფასი შენატანს შემოსავლების სამსახურში. 
    თუ შემოსავალი აღემატება 1000 ლარს მაშინ შესატანია 20%, 
    ხოლო თუ 1000-ზე ნაკლებია მაშინ შენატანი 0-ია. 
    ეკრანზე გამოიტანეთ როგორც შესატანი თანხის მოხულობა, ისე ხელზე ასაღები თანხა.
*/

#include <iostream>
#include <string>

using namespace std;


int main() {
    int salary;
    cout << "Enter your salary: ";
    cin >> salary;
    

    if(salary > 1000) {
        cout << "Tax: " << salary * 0.2 << endl;
        cout << "On hand: " << salary - (salary*0.2) << endl;
    } else {
        cout << "You dont have to pay tax." << endl;
    }


    return 0;
}