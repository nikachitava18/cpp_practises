/*
Task 3
    სტუდენტი გადის ექვს საგანს. ასახეთ ექვსივე საგნის საბოლოო ქულა. გამოითვალეთ საშუალო.
    თუ საშუალო 91-იდან 100-მდე არის მაშინ დაწერეთ შეფასება A, თუ 81-იდან 90-ის ჩათვლით მაშინ B, 
    თუ 71-იდან 80-ის ჩათვლით მაშინ C, თუ 61-იდან 70-ის ჩათვლით მაშინ D, 
    თუ 51-იდან 60-ის ჩათვლით მაშინ E, თუ 51-ზე ნაკლები მაშინ F 
    და დამატებით შეტყობინება რომ ვერ გაიარა საგანი.
*/

#include <iostream>
#include <string>

using namespace std;


int main() {

    int l_1, l_2, l_3, l_4, l_5, l_6;
    
    cout << "Enter your 1st lecture mark: ";
    cin >> l_1;
    cout << "Enter your 2nd lecture mark: ";
    cin >> l_2;
    cout << "Enter your 3rd lecture mark: ";
    cin >> l_3;
    cout << "Enter your 4th lecture mark: ";
    cin >> l_4;
    cout << "Enter your 5th lecture mark: ";
    cin >> l_5;
    cout << "Enter your 6th lecture mark: ";
    cin >> l_6;

    double average = (l_1 + l_2 + l_3 + l_4 + l_5 + l_6) / 6;

    cout << "Your average mark: " << average << endl;
    if(average >= 91 && average <= 100) {
        cout << "You get mark: A" << endl;
    } else if (average >= 81 && average <= 90) {
        cout << "You get mark: B" << endl;
    } else if (average >= 71 && average <= 80) {
        cout << "You get mark: C" << endl;
    } else if (average >= 61 && average <= 70) {
        cout << "You get mark: D" << endl;
    } else if (average >= 51 && average <= 60) {
        cout << "You get mark: E" << endl;
    } else if(average < 51) {
        cout << "You get mark: F" << endl;
        cout << "You can not pass the subjerct" << endl;
    }
    return 0;
}