/*
Task 4
    აღწერეთ სამი რიცხვი. 
    დაალაგეთ შედარების ოპერატორის გამოყენებით ეს სამი რიცხვი ზრდადობით და დაბეჭდეთ ეკრანზე. 
*/

#include <iostream>
#include <string>

using namespace std;


int main() {
    
    int a, b, c;
    cout << "Enter 1st number: ";
    cin >> a;
    cout << "Enter 2nd number: ";
    cin >> b;
    cout << "Enter 3rd number: ";
    cin >> c;

    if (a > b && a > c && b > c){
        cout << c << " " << b << " " << a  << endl;
    } else if (a > b && a > c && c > b) {
        cout << b << " " << c << " " << a << endl;
    } else if (b > a && b > c && a > c) {
        cout << c << " " << a << " " << b << endl;
    } else if (b > a && b > c && c > a) {
        cout << a << " " << c << " " << b << endl;
    } else if (c > a && c > b && a > b) {
        cout << b << " " << a << " " << c << endl;
    } else {
        cout << a << " " << b << " " << c << endl;
    }

    return 0;
}