#include <iostream>
#include <string>

using namespace std;

/* Find maximum member of an array using Recursion In C++ */

int findMax(int array[], int n) {

    if(n == 1) {
        return array[0];
    } else {
        return max(array[n-1], findMax(array, n-1));
    }
}


int main() {

    int a[5] = {3, -2, 12, 32, 5};
    int arr_size = sizeof(a)/sizeof(a[0]);
    cout << "max: " <<findMax(a, arr_size) << endl;
    
    return 0;
}