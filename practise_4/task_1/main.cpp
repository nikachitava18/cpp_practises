#include <iostream>
#include <string>

using namespace std;

/* Checking Whether The Number Is Prime Or Not Using Recursion In C++ */

bool checkPrime(int x, int i = 2) {
    if(x == 1) {
        return false;
    } else if(x == 2) {
        return true;
    } else if(x % i == 0) {
        return false;
    } else if (i * i > x) {
        return true;
    }
    return checkPrime(x, i +1);
}

int main() {
    
    int x;
    cout << "Enter number: ";
    cin >> x;

    cout << checkPrime(x) << endl;

    return 0;
}