#include <iostream>
#include <string>

using namespace std;

// Reverse A Number Using Recursion In C++

void reversNum(int x) {
    if(x < 10) {
        cout << x;
        return;
    } else {
        cout << x % 10;
        reversNum(x/10);
    }
} 

main () {

    int a;
    cout << "Enter number:";
    cin >> a;
    reversNum(a);

    return 0;
}